#!MC 1400
# Created by Tecplot 360 build 14.0.2.35002
$!VarSet |MFBD| = '.'
$!PICK SETMOUSEMODE
  MOUSEMODE = SELECT
$!PICK SETMOUSEMODE
  MOUSEMODE = SELECT
$!OPENLAYOUT  ".\case3_temperature1.lpk"
$!LINEMAP [1]  SYMBOLS{FILLMODE = USELINECOLOR}
$!READDATASET  '.\T1.dat '
  READDATAOPTION = APPEND
  RESETSTYLE = NO
  INCLUDETEXT = NO
  INCLUDEGEOM = NO
  INCLUDECUSTOMLABELS = NO
  VARLOADMODE = BYNAME
  ASSIGNSTRANDIDS = YES
  VARNAMELIST = '"z/L" "U/Umax" "Tstar"'
$!ACTIVELINEMAPS -= [3]
$!ACTIVELINEMAPS -= [2]
$!LINEMAP [1]  NAME = 'experiment'
$!CREATELINEMAP 
$!LINEMAP [5]  NAME = 'simulation'
$!LINEMAP [5]  ASSIGN{XAXISVAR = 3}
$!LINEMAP [5]  ASSIGN{YAXISVAR = 1}
$!LINEMAP [5]  ASSIGN{ZONE = 55}
$!LINEMAP [5]  INDICES{IJKLINES = I}
$!ACTIVELINEMAPS += [5]
$!LINEMAP [3]  SYMBOLS{SHOW = NO}
$!LINEMAP [5]  SYMBOLS{SHOW = NO}
$!LINEMAP [5]  LINES{COLOR = BLACK}
$!LINEMAP [5]  LINES{LINETHICKNESS = 0.40000000000000002}
$!REDRAWALL 
$!LINEMAP [5]  LINES{LINEPATTERN = DASHDOTDOT}
$!REDRAWALL
$!XYLINEAXIS XDETAIL 1 {TITLE{OFFSET = 5}}
$!XYLINEAXIS XDETAIL 1 {TITLE{TEXT = 'T<sup>*</sup>'}} 
$!FRAMELAYOUT WIDTH = 5
$!FRAMELAYOUT SHOWBORDER = NO
$!XYLINEAXIS VIEWPORTPOSITION{X2 = 90}
$!XYLINEAXIS VIEWPORTPOSITION{Y2 = 95}
$!GLOBALLINEPLOT LEGEND{XYPOS{X = 87}}
$!GLOBALLINEPLOT LEGEND{TEXTSHAPE{HEIGHT = 18}}
$!EXPORTSETUP EXPORTFORMAT = EPS
$!EXPORTSETUP EXPORTREGION = ALLFRAMES
$!EXPORTSETUP IMAGEWIDTH = 600
$!EXPORTSETUP EXPORTFNAME = '.\T1.eps'
$!EXPORT 
  EXPORTREGION = ALLFRAMES
$!EXPORTSETUP EXPORTFORMAT = PNG
$!EXPORTSETUP EXPORTREGION = ALLFRAMES
$!EXPORTSETUP IMAGEWIDTH = 1800
$!EXPORTSETUP EXPORTFNAME = '.\T1.png'
$!EXPORT 
  EXPORTREGION = ALLFRAMES
$!EXPORTSETUP EXPORTFORMAT = JPEG
$!EXPORTSETUP EXPORTREGION = ALLFRAMES
$!EXPORTSETUP IMAGEWIDTH = 1800
$!EXPORTSETUP EXPORTFNAME = '.\T1.jpeg'
$!EXPORT 
  EXPORTREGION = ALLFRAMES
$!RemoveVar |MFBD|
$!Quit