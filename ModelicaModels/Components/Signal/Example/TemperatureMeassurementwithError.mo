within ModelicaModels.Components.Signal.Example;
model TemperatureMeassurementwithError
  "Temperature Meassuremen twithError"
  extends Modelica.Icons.Example;
  replaceable package MediumA = Buildings.Media.Air;
  Buildings.Fluid.Sources.MassFlowSource_T boundary(use_T_in=true, nPorts=1,
    redeclare package Medium = MediumA,
    m_flow=1)
    annotation (Placement(transformation(extent={{-46,-48},{-26,-28}})));
  Buildings.Fluid.Sources.FixedBoundary bou(nPorts=1, redeclare package Medium =
        MediumA)
    annotation (Placement(transformation(extent={{76,-48},{56,-28}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senTem(
    redeclare package Medium = MediumA,
    m_flow_nominal=1,
    T_start(displayUnit="K"))
    annotation (Placement(transformation(extent={{-18,-48},{2,-28}})));
  Modelica.Blocks.Sources.Ramp ramp(
    duration=500,
    offset=273.15 + 25,
    height=10)
    annotation (Placement(transformation(extent={{-92,-44},{-72,-24}})));
  PlusNormallyDistributedError plusNormallyDistributedError(samplePeriod=senTem.tau, nInputs=
        2)
    annotation (Placement(transformation(extent={{2,18},{22,38}})));
  Modelica.Blocks.Sources.Ramp ramp1(
    duration=500,
    offset=273.15 + 25,
    height=5)
    annotation (Placement(transformation(extent={{-92,-84},{-72,-64}})));
  Modelica.Blocks.Routing.Multiplex2 multiplex2_1
    annotation (Placement(transformation(extent={{-38,18},{-18,38}})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary1(
    use_T_in=true,
    nPorts=1,
    redeclare package Medium = MediumA,
    m_flow=1)
    annotation (Placement(transformation(extent={{-46,-88},{-26,-68}})));
  Buildings.Fluid.Sources.FixedBoundary bou1(nPorts=1, redeclare package Medium =
        MediumA)
    annotation (Placement(transformation(extent={{76,-88},{56,-68}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senTem1(
    redeclare package Medium = MediumA,
    m_flow_nominal=1,
    T_start(displayUnit="K"))
    annotation (Placement(transformation(extent={{-18,-88},{2,-68}})));
equation
  connect(boundary.ports[1], senTem.port_a)
    annotation (Line(points={{-26,-38},{-18,-38}},
                                               color={0,127,255}));
  connect(senTem.port_b, bou.ports[1])
    annotation (Line(points={{2,-38},{56,-38}},
                                            color={0,127,255}));
  connect(boundary1.ports[1], senTem1.port_a)
    annotation (Line(points={{-26,-78},{-18,-78}}, color={0,127,255}));
  connect(senTem1.port_b, bou1.ports[1])
    annotation (Line(points={{2,-78},{56,-78}}, color={0,127,255}));
  connect(ramp.y, boundary.T_in)
    annotation (Line(points={{-71,-34},{-48,-34}}, color={0,0,127}));
  connect(ramp1.y, boundary1.T_in)
    annotation (Line(points={{-71,-74},{-48,-74}}, color={0,0,127}));
  connect(senTem1.T, multiplex2_1.u2[1]) annotation (Line(points={{-8,-67},{-62,
          -67},{-62,22},{-40,22}}, color={0,0,127}));
  connect(senTem.T, multiplex2_1.u1[1]) annotation (Line(points={{-8,-27},{-8,2},
          {-68,2},{-68,34},{-40,34}}, color={0,0,127}));
  connect(multiplex2_1.y, plusNormallyDistributedError.u)
    annotation (Line(points={{-17,28},{0,28}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end TemperatureMeassurementwithError;
