within ModelicaModels.Components.Signal;
model PlusNormallyDistributedError
  parameter Integer nInputs = 1 "Dimension of input signal";
  extends Modelica.Blocks.Interfaces.MIMO(final nin=nInputs, final nout=nin);
  parameter Real mu=0 "Expectation (mean) value of the normal distribution";
  parameter Real sigma = 1 "Standard deviation";
  parameter Modelica.SIunits.Period samplePeriod=0.001
    "Period for sampling the raw random numbers";
  Modelica.Blocks.Noise.NormalNoise normalNoise(
    mu=mu,
    sigma=sigma,
    samplePeriod=samplePeriod)
    annotation (Placement(transformation(extent={{-44,30},{-24,50}})));

  inner Modelica.Blocks.Noise.GlobalSeed globalSeed
    annotation (Placement(transformation(extent={{20,30},{40,50}})));
equation
  for i in 1:nInputs loop
    y[i] = u[i]+normalNoise.y;
  end for;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end PlusNormallyDistributedError;
