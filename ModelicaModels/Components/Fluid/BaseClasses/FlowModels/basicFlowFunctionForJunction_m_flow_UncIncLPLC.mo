within ModelicaModels.Components.Fluid.BaseClasses.FlowModels;
function basicFlowFunctionForJunction_m_flow_UncIncLPLC
  "Function that computes pressure drop between branches of a junction for given mass flow rate using inconstant Pressure Loss Coefficient with uncertain correction"
  import Buildings;
  input Modelica.SIunits.Velocity V_up
    "Velocity in upstream section";
  input Modelica.SIunits.Velocity V_down
    "Velocity in downstream section";
  input Modelica.SIunits.Density rho "Density of the medium, default is air";
  input Real PolyCoef[:]
    "Coefficients of the polynomial model of local pressure loss coefficient,from zero order to higher orders";
  input Real CorFac
    "Correction factor for uncertainty consideration";
  output Modelica.SIunits.PressureDifference dp(displayUnit="Pa")
    "Pressure difference between port_a and port_b (= port_a.p - port_b.p)";

protected
  Modelica.SIunits.Pressure DynPre(displayUnit="Pa") = 0.5*rho*V_up^2
  "Dynamic pressure of upstream section";
  Real VelRat=V_down*
      Buildings.Utilities.Math.Functions.inverseXRegularized(
      V_up, 10e-5) "Velocity ratio of V_down to V_up";
  Real LPLC=
      Buildings.Utilities.Math.Functions.polynomial(
      x=VelRat, a=PolyCoef);
algorithm
  dp:=CorFac*LPLC*DynPre;

end basicFlowFunctionForJunction_m_flow_UncIncLPLC;
