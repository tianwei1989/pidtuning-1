within ModelicaModels.Components.Fluid.Junction;
model BranchResistance_IncLPLC
  "Inconstant LPLC with dp and m_flow as parameter"
  import Buildings;
  extends Buildings.Fluid.BaseClasses.PartialResistance(
      final m_flow_turbulent=if computeFlowResistance then 0.05*
        m_flow_nominal_pos else 0);
  final parameter Modelica.SIunits.Density rho = Medium.density(sta_default) "Density of the medium, default is air";
  parameter Real PolyCoef[:] = {1,2,3,4}
    "Coefficients of the polynomial model of local pressure loss coefficient,from zero order to higher orders";

protected
  final parameter Boolean computeFlowResistance=(dp_nominal_pos > Modelica.Constants.eps)
    "Flag to enable/disable computation of flow resistance"
   annotation(Evaluate=true);
public
  Modelica.Blocks.Interfaces.RealInput V_up "Velocity of upstream branch"
    annotation (Placement(transformation(extent={{-140,44},{-100,84}}),
        iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=-90,
        origin={-79,51})));
public
  Modelica.Blocks.Interfaces.RealInput V_down "Velocity of downstream branch"
    annotation (Placement(transformation(extent={{-140,8},{-100,48}}),
        iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=-90,
        origin={81,51})));

initial equation
 assert(m_flow_nominal_pos > 0, "m_flow_nominal_pos must be non-zero. Check parameters.");
 assert(linearized == false, "Linearized option must be false. Check parameters.");
 assert(from_dp == false, "Calculation must be from m_flow. Check parameters.");

equation
  // Pressure drop calculation
  if computeFlowResistance then
      if homotopyInitialization then
      dp = homotopy(actual=
        BaseClasses.FlowModels.basicFlowFunctionForJunction_m_flow_inconstantLPLC(
        V_up=V_up,
        V_down=V_down,
        rho=rho,
        PolyCoef=PolyCoef), simplified=dp_nominal_pos*m_flow/m_flow_nominal_pos);
      else // do not use homotopy
      dp =
        BaseClasses.FlowModels.basicFlowFunctionForJunction_m_flow_inconstantLPLC(
        V_up=V_up,
        V_down=V_down,
        rho=rho,
        PolyCoef=PolyCoef);
      end if; // homotopyInitialization
  else // do not compute flow resistance
    dp = 0;
  end if;  // computeFlowResistance

  annotation (Icon(graphics={Text(
          extent={{-58,74},{-26,54}},
          lineColor={28,108,200},
          textString="V_up"), Text(
          extent={{14,70},{66,38}},
          lineColor={28,108,200},
          textString="V_down
")}));
end BranchResistance_IncLPLC;
