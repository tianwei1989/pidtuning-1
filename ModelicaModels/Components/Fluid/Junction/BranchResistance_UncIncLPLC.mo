within ModelicaModels.Components.Fluid.Junction;
model BranchResistance_UncIncLPLC
  "Uncertain inconstant LPLC with dp and m_flow as parameter"
  import Buildings;
  Buildings.Fluid.Actuators.BaseClasses.ActuatorSignal dpFilter(
   final y_start=_dp_start,
    use_inputFilter=use_inputFilter,
    riseTime=riseTime)
  annotation (Placement(transformation(extent={{-86,66},{-66,86}})));
  extends Buildings.Fluid.BaseClasses.PartialResistance(
      final m_flow_turbulent=if computeFlowResistance then 0.05*
        m_flow_nominal_pos else 0);
  final parameter Modelica.SIunits.Density rho = Medium.density(sta_default) "Density of the medium, default is air";
  parameter Real PolyCoef[:] = {1,2,3,4}
    "Coefficients of the polynomial model of local pressure loss coefficient,from zero order to higher orders";
  parameter Real CorFac = 1
    "Correction factor for uncertainty";
  Modelica.SIunits.PressureDifference dp_target(displayUnit="Pa");

protected
  final parameter Boolean computeFlowResistance=(dp_nominal_pos > Modelica.Constants.eps)
    "Flag to enable/disable computation of flow resistance"
   annotation(Evaluate=true);
public
  Modelica.Blocks.Interfaces.RealInput V_up "Velocity of upstream branch"
    annotation (Placement(transformation(extent={{-140,44},{-100,84}}),
        iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=-90,
        origin={-79,51})));
public
  Modelica.Blocks.Interfaces.RealInput V_down "Velocity of downstream branch"
    annotation (Placement(transformation(extent={{-140,8},{-100,48}}),
        iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=-90,
        origin={81,51})));

  Modelica.Blocks.Sources.RealExpression dp_1(y=dp_target)
    annotation (Placement(transformation(extent={{-26,76},{-46,96}})));
  Modelica.Blocks.Interfaces.RealOutput dp_actual
  "Actual valve position"
    annotation (Placement(transformation(extent={{100,50},{120,70}})));
  parameter Boolean use_inputFilter=true
    "= true, if opening is filtered with a 2nd order CriticalDamping filter"
    annotation(Dialog(tab="Dynamics", group="Filtered dp"));
  parameter Modelica.SIunits.Time riseTime=120
    "Rise time of the filter (time to reach 99.6 % of an opening step)"
    annotation(Dialog(tab="Dynamics", group="Filtered dp",enable=use_inputFilter));
initial equation
 assert(m_flow_nominal_pos > 0, "m_flow_nominal_pos must be non-zero. Check parameters.");
 assert(linearized == false, "Linearized option must be false. Check parameters.");
 assert(from_dp == false, "Calculation must be from m_flow. Check parameters.");

equation

  // Pressure drop calculation
  if computeFlowResistance then
      if homotopyInitialization then
      dp_target =  homotopy(actual=
        BaseClasses.FlowModels.basicFlowFunctionForJunction_m_flow_UncIncLPLC(
        V_up=V_up,
        V_down=V_down,
        rho=rho,
        PolyCoef=PolyCoef,
        CorFac=CorFac), simplified=dp_nominal_pos*m_flow/m_flow_nominal_pos);
      else // do not use homotopy
      dp_target =
        BaseClasses.FlowModels.basicFlowFunctionForJunction_m_flow_UncIncLPLC(
        V_up=V_up,
        V_down=V_down,
        rho=rho,
        PolyCoef=PolyCoef,
        CorFac=CorFac);
      end if; // homotopyInitialization
  else // do not compute flow resistance
    dp_target = 0;

  end if;  // computeFlowResistance

  dp=dp_actual;
  connect(dp_1.y, dpFilter.y) annotation (Line(points={{-47,86},{-60,86},{-60,96},{-76,96},{-76,88}},
                              color={0,0,127}));
  connect(dpFilter.y_actual,dp_actual)  annotation (Line(points={{-71,83},{-60,83},
          {-60,60},{110,60}}, color={0,0,127}));
  annotation (Icon(graphics={Text(
          extent={{-58,74},{-26,54}},
          lineColor={28,108,200},
          textString="V_up"), Text(
          extent={{14,70},{66,38}},
          lineColor={28,108,200},
          textString="V_down
")}));
end BranchResistance_UncIncLPLC;
