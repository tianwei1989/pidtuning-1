within ModelicaModels.System;
model TransferFunction4thOrderPlusDelay
  "Calculate the output profile of a specified Transfer function"
  extends Modelica.Icons.Example;
  parameter String TransferFunInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/TransferFunInput.txt");
  parameter Real K = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "K")
    "Scale factor K";
  parameter Real T1 = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "T1")
    "1st Integral constant";
  parameter Real T2 = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "T2")
    "2nd Integral constant";
  parameter Real T3 = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "T3")
    "3rd Integral constant";
  parameter Real T4 = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "T4")
    "4th Integral constant";
  parameter Real D = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "D")
    "Delay time";
  parameter Real IniVal = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "InitialValue")
    "Delay time";
  Components.Signal.BumpStep bumpStep(
    height=-0.7,
    startTime1st=800,
    startTime2nd=2000,
    offset=1) annotation (Placement(transformation(extent={{-96,94},{-76,114}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder(T=T1,k=K,
    initType=Modelica.Blocks.Types.Init.SteadyState)
    annotation (Placement(transformation(extent={{-62,94},{-42,114}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder1(T=T2,
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-26,94},{-6,114}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay(delayTime=D)
    annotation (Placement(transformation(extent={{102,94},{122,114}})));
  Modelica.Blocks.Sources.Constant Const(k=IniVal - K)
    "Initial value of the process"
    annotation (Placement(transformation(extent={{102,52},{122,72}})));
  Modelica.Blocks.Math.Add FinalValue
    annotation (Placement(transformation(extent={{152,70},{172,90}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder2(
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0,
    T=T3)
    annotation (Placement(transformation(extent={{16,94},{36,114}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder3(
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0,
    T=T4)
    annotation (Placement(transformation(extent={{54,94},{74,114}})));
equation
  connect(bumpStep.y, firstOrder.u)
    annotation (Line(points={{-75,104},{-64,104}},
                                               color={0,0,127}));
  connect(firstOrder.y, firstOrder1.u)
    annotation (Line(points={{-41,104},{-28,104}},
                                           color={0,0,127}));
  connect(fixedDelay.y, FinalValue.u1) annotation (Line(points={{123,104},{140,104},
          {140,86},{150,86}},color={0,0,127}));
  connect(Const.y, FinalValue.u2) annotation (Line(points={{123,62},{142,62},{
          142,74},{150,74}}, color={0,0,127}));
  connect(firstOrder1.y, firstOrder2.u)
    annotation (Line(points={{-5,104},{14,104}}, color={0,0,127}));
  connect(firstOrder2.y, firstOrder3.u)
    annotation (Line(points={{37,104},{52,104}}, color={0,0,127}));
  connect(firstOrder3.y, fixedDelay.u)
    annotation (Line(points={{75,104},{100,104}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {200,160}})),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{200,160}})));
end TransferFunction4thOrderPlusDelay;
