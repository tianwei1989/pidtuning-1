within ModelicaModels.System;
model TransferFunction2ndOrderPlusDelay
  "Calculate the output profile of a specified Transfer function"
  extends Modelica.Icons.Example;
  parameter String TransferFunInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/TransferFunInput.txt");
  parameter Real K = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "K")
    "Scale factor K";
  parameter Real T1 = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "T1")
    "1st Integral constant";
  parameter Real T2 = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "T2")
    "2nd Integral constant";
  parameter Real D = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "D")
    "Delay time";
  parameter Real IniVal = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "InitialValue")
    "Delay time";
  Components.Signal.BumpStep bumpStep(
    height=-0.7,
    startTime1st=800,
    startTime2nd=2000,
    offset=1) annotation (Placement(transformation(extent={{-60,62},{-40,82}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder(T=T1,k=K,
    initType=Modelica.Blocks.Types.Init.SteadyState)
    annotation (Placement(transformation(extent={{-18,62},{2,82}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder1(T=T2,
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{22,62},{42,82}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay(delayTime=D)
    annotation (Placement(transformation(extent={{64,62},{84,82}})));
  Modelica.Blocks.Sources.Constant Const(k=IniVal - K)
    "Initial value of the process"
    annotation (Placement(transformation(extent={{64,20},{84,40}})));
  Modelica.Blocks.Math.Add FinalValue
    annotation (Placement(transformation(extent={{114,38},{134,58}})));
equation
  connect(bumpStep.y, firstOrder.u)
    annotation (Line(points={{-39,72},{-20,72}},
                                               color={0,0,127}));
  connect(firstOrder.y, firstOrder1.u)
    annotation (Line(points={{3,72},{20,72}},
                                           color={0,0,127}));
  connect(fixedDelay.y, FinalValue.u1) annotation (Line(points={{85,72},{102,72},
          {102,54},{112,54}},color={0,0,127}));
  connect(Const.y, FinalValue.u2) annotation (Line(points={{85,30},{104,30},{
          104,42},{112,42}}, color={0,0,127}));
  connect(firstOrder1.y, fixedDelay.u)
    annotation (Line(points={{43,72},{62,72}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {200,160}})),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{200,160}})));
end TransferFunction2ndOrderPlusDelay;
