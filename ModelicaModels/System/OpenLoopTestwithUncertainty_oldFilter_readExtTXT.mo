within ModelicaModels.System;
model OpenLoopTestwithUncertainty_oldFilter_readExtTXT
  "simulate mixed convection in a room with a box inside"
  extends Modelica.Icons.Example;
  //Read external uncertainty parameters
  parameter String risTimInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/risTimInput.txt");
  parameter String PLCUncInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/PLCUncInput.txt");
  parameter String bouTemInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/bouTemInput.txt");

  parameter Real UncRisTim(unit="s") = Modelica.Utilities.Examples.readRealParameter(risTimInput, "riseTime") "Uncertain rise time";
  parameter Real UncFacPLC = Modelica.Utilities.Examples.readRealParameter(PLCUncInput, "UncFacPLC")
    "Uncertainty factor,multiplied by the original flow coefficient of damper";
  parameter Modelica.SIunits.Temperature UncTOthWal(displayUnit="degC") = Modelica.Utilities.Examples.readRealParameter(bouTemInput, "Wall")
    "Fixed temperature at port";
  parameter Modelica.SIunits.Temperature UncTFlo(displayUnit="degC") = Modelica.Utilities.Examples.readRealParameter(bouTemInput, "Floor")
    "Fixed temperature at port";
  parameter Modelica.SIunits.Temperature UncTCei(displayUnit="degC") = Modelica.Utilities.Examples.readRealParameter(bouTemInput, "Ceiling")
    "Fixed temperature at port";
extends Components.Room.MixedConvectionBase(
    roo(
      surBou(
        name={"East Wall","West Wall","North Wall","South Wall","Ceiling","Floor"},
        A={5.7584,5.8804,5.9536,5.9536,5.9536,5.9536},
        til={Buildings.Types.Tilt.Wall,Buildings.Types.Tilt.Wall,Buildings.Types.Tilt.Wall,
            Buildings.Types.Tilt.Wall,Buildings.Types.Tilt.Ceiling,Buildings.Types.Tilt.Floor},
        each boundaryCondition=Buildings.ThermalZones.Detailed.Types.CFDBoundaryConditions.Temperature),
      cfdFilNam="Resources/Data/Rooms/FFD/MixedConvectionWithBox16sensors.ffd",
      intConMod=Buildings.HeatTransfer.Types.InteriorConvection.Fixed,
      sensorName={"occupant average T","occupant average V","thermostat1",
      "thermostat2","thermostat3","thermostat4","thermostat5","thermostat6",
      "thermostat7","thermostat8","thermostat9","thermostat10","thermostat11",
      "thermostat12","thermostat13","thermostat14","thermostat15","thermostat16"},
      p_start(displayUnit="Pa") = 101325,
      nPorts=2,
      samplePeriod=2,
      massDynamics=Modelica.Fluid.Types.Dynamics.SteadyStateInitial),
    nSurBou=6,
    nOthWal=4,
    TOthWal(each T(displayUnit="degC") = UncTOthWal),
    TFlo(T=UncTFlo),
    bouOut(use_T=false, p(displayUnit="Pa")));

   Buildings.HeatTransfer.Sources.FixedTemperature Tcei(T(displayUnit="degC")=
         UncTCei)
    "temperature of ceiling"                                            annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={188,-144})));

  Components.Signal.BumpStep BumpStep
    annotation (Placement(transformation(extent={{-80,-100},{-60,-80}})));
  Buildings.Fluid.Sources.FixedBoundary bouIn(
    redeclare package Medium = MediumA,
    use_T=true,
    p(displayUnit="Pa") = 101325 + 20,
    T=289.15,
    nPorts=1) annotation (Placement(transformation(extent={{-12,-122},{8,-102}})));
  Buildings.Fluid.Actuators.Dampers.VAVBoxExponential vavDam(
    redeclare package Medium = MediumA,
    m_flow_nominal=0.09952,
    dp_nominal=20,
    v_nominal=3,
    y_start=0.3,
    riseTime=UncRisTim)
    annotation (Placement(transformation(extent={{30,-122},{50,-102}})));


  Components.Signal.PlusNormallyDistributedError plusNormallyDistributedError(
                 samplePeriod=roo.samplePeriod,sigma=0.5,nInputs=16)
    annotation (Placement(transformation(extent={{178,-36},{198,-16}})));

/*initial algorithm 
UncRisTim:= Modelica.Utilities.Examples.readRealParameter(risTimInput, "riseTime");
UncFacPLC:= Modelica.Utilities.Examples.readRealParameter(PLCUncInput, "UncFacPLC");
UncTOthWal:=Modelica.Utilities.Examples.readRealParameter(bouTemInput, "Wall");
UncTFlo:=Modelica.Utilities.Examples.readRealParameter(bouTemInput, "Floor");
UncTCei:=Modelica.Utilities.Examples.readRealParameter(bouTemInput, "Ceiling");*/

equation
  connect(Tcei.port, roo.surf_surBou[5]) annotation (Line(
      points={{178,-144},{134.2,-144},{134.2,-30}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(bouIn.ports[1], vavDam.port_a)
    annotation (Line(points={{8,-112},{30,-112}}, color={0,127,255}));
  connect(vavDam.port_b, roo.ports[1]) annotation (Line(points={{50,-112},{86,-112},
          {86,-26},{123,-26}}, color={0,127,255}));
  connect(doubletStep.y, vavDam.y)
    annotation (Line(points={{-59,-90},{40,-90},{40,-100}}, color={0,0,127}));
  connect(roo.yCFD[3:18], plusNormallyDistributedError.u[1:16]) annotation (Line(points={{159,
          -4},{162,-4},{162,-26},{176,-26}}, color={0,0,127}));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-160},{240,140}})),
            experiment(StopTime=40),
   Documentation(info="<html> 
<p align=\"left\">
</html>", revisions="<html>
<ul>
<li>   
December 31, 2013, by Wangda Zuo:<br/>
First implementation.
</li>
</ul>
</html>"),
    Icon(coordinateSystem(extent={{-120,-160},{240,140}})));
end OpenLoopTestwithUncertainty_oldFilter_readExtTXT;
