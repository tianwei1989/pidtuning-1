# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 16:12:02 2018

@author: Administrator
"""
import numpy as np
import sys
import os
from buildingspy.io.outputfile import Reader 
import pandas as pd
import shutil
#from simulation import translate
from pso import pso
import matplotlib.pyplot as plt
#import timeit
import multiprocessing as mp
import math
def constraint(x,*args):
    t1 = x[0]
    t2 = x[1]
    return [t1-t2]


def transferFunctionPython(repoPath,baselineValue,step,startTime,period,timestep):
    #Using another modelica model "Transfer function" to calculate the simulated output 
    #then using PSO to identify the proper parameters
    #repoPath is the directory of the repository
    #baselineValue is the raw data from coupled FFD simulation
    #step is the size of the step signal
    #startTime is the starting time when the excitation signal is introduced
    #period is the time period during which the identification is performed
    #timestep is the time step size of sampling
    
    T1 = []
    T2 = []
    D = []
    Fopt = []
    #Calculate K manually
    K=(baselineValue[(baselineValue.Time==startTime+period)].iloc[:,1].values-baselineValue[(baselineValue.Time==startTime)].iloc[:,1].values)/step
    #Define the other parameters in the objective function
    args = (repoPath,baselineValue,K[-1],startTime,period,timestep,step)
    #Define the lower and upper boundary for the parameters
    lb = [0.01,0.01,0]
    ub = [500,300,100]

    for i in range(3): # Perform 5 times of PSO to get a best result
#        xopt,fopt = pso(objectiveFunctionDymola,lb,ub,f_ieqcons=constraint,args = args, debug=True)
        xopt,fopt = pso(objectiveFunctionPython,lb,ub,f_ieqcons = constraint,args = args)
    #    K.append(xopt[0])
        T1.append(xopt[0])
        T2.append(xopt[1])
        D.append(xopt[2])
        Fopt.append(fopt)
    Kopt = K[-1]
    T1opt = T1[Fopt.index(min(Fopt))]
    T2opt= T2[Fopt.index(min(Fopt))]
    Dopt = D[Fopt.index(min(Fopt))]
    fopt = min(Fopt)

    comparisonPython(Kopt,T1opt,T2opt,Dopt,step,baselineValue,startTime,period,timestep)

    return Kopt,T1opt,T2opt,Dopt,fopt


def objectiveFunctionPython(x,*args):
    T1, T2, D = x
    repoPath, meaDF, K, startTime, period, timestep,step = args
    
    ## Calculate the initial value at the startTime
    IniVal = meaDF[meaDF.Time==startTime].iloc[:,1].values
    # Create time series
    realtime = np.arange(startTime,startTime+period+timestep,timestep)
    timetag = realtime-startTime
    # Calculate corresponding output of transfer function
    # vectorize transfer function
    V2ndOrderPlusDelay = np.vectorize(SecondOrderPlusDelay,otypes=[np.float])
    output = V2ndOrderPlusDelay(K, T1, T2, D, step, timetag)+IniVal
    DF = {'Time':realtime,'simTemp':output}
    resultDF = pd.DataFrame(data = DF)

    #simDF and meaDF are both type of series
    #Slice the data after the startime and within the transition period
    simDF = resultDF[(resultDF.Time>=startTime)&(resultDF.Time<=startTime+period)]
    meaDF = meaDF[(meaDF.Time>=startTime)&(meaDF.Time<=startTime+period)]
    #Drop dupilicates
    simDF = simDF.drop_duplicates("Time")
    meaDF = meaDF.drop_duplicates("Time")
    if np.size(simDF,axis=0) != np.size(meaDF,axis=0):
        sys.exit("The dimensions of simDF and MeaDF don't match with each other!")
   
    #Calculate the error between the simulated value using transfer function and the coupled FFD simulation
    #Both the error of the points and the error of slope between two adjacent points are taken into account 
    absError = np.sum(np.square(simDF.iloc[:,1].values-meaDF.iloc[:,1].values))
    slope_sim = np.zeros([np.size(simDF)-1,1])
    slope_mea = np.zeros([np.size(meaDF)-1,1])
    slope_sim = (simDF.iloc[1:,1].values-simDF.iloc[:-1,1].values)/timestep
    slope_mea = (meaDF.iloc[1:,1].values-meaDF.iloc[:-1,1].values)/timestep
    slopeError = np.sum(np.abs(slope_sim-slope_mea))
    objVal = np.log(absError)+np.log(slopeError)
    
    return objVal
    
def SecondOrderPlusDelay(K,T1,T2,D,step,time):
    if time <= D:
        output = 0
    else:
        if T2-T1 == 0:
            output = 0
        else:
            output = step*K*(1+T1*T2/(T2-T1)*(np.exp(-(time-D)/T1)/T2-np.exp(-(time-D)/T2)/T1));
        
    return output;
            
def comparisonPython(K,T1,T2,D,step,meaDF,startTime,period,timestep):
    ## Calculate the initial value at the startTime
    IniVal = meaDF[meaDF.Time==startTime].iloc[:,1].values
    # Create time series
    realtime = np.arange(startTime,startTime+period+timestep,timestep)
    timetag = realtime-startTime
    # Calculate corresponding output of transfer function
    # vectorize transfer function
    V2ndOrderPlusDelay = np.vectorize(SecondOrderPlusDelay,otypes=[np.float])
    output = V2ndOrderPlusDelay(K, T1, T2, D,step, timetag)+IniVal
    DF = {'Time':realtime,'simTemp':output}
    resultDF = pd.DataFrame(data = DF)
    
    #simDF and meaDF are both type of series
    #Slice the data after the startime and within the transition period
    simDF = resultDF[(resultDF.Time>=startTime)&(resultDF.Time<=startTime+period)]
    meaDF = meaDF[(meaDF.Time>=startTime)&(meaDF.Time<=startTime+period)]
    #Drop dupilicates
    simDF = simDF.drop_duplicates("Time")
    meaDF = meaDF.drop_duplicates("Time")
    if np.size(simDF,axis=0) != np.size(meaDF,axis=0):
        sys.exit("The dimensions of simDF and MeaDF don't match with each other!")
    
     # Plot simDF and meaDF
    fig,ax = plt.subplots(1,1,figsize=(10,6))
    ax.plot(simDF["Time"].values,simDF.iloc[:,1].values,'b',label = "Transfer Function")
    ax.plot(meaDF["Time"].values,meaDF.iloc[:,1].values,'r',label = "FFD simulation")
    ax.legend()
    ax.set_xlabel('Time/s')
    ax.set_ylabel(meaDF.columns[1]+'/K')            
    plt.show()


# Main script
def optimization(repoPath,ScenarioNum,result,output):
#    Start = timeit.default_timer()
    
    ResultPath = repoPath+'/Results'
#    ## copy .mat result file from scenario folder to a temp folder
    ResultFile = os.path.join(ResultPath,"Scenario-"+str(ScenarioNum+1),
                                  result)
    
#    
    ## perform pso and generate .csv file
    time = []
    AveTemp = []
    r = Reader(ResultFile,"dymola")
    (time,AveTemp) = r.values('roo.yCFD[1]')
    LocTemp = np.zeros([np.size(time,axis=0),16])
    #Loop in 16 local temperature sensors
    for j in range(16):
        (time,loctemp) = r.values('roo.yCFD['+str(j+3)+']')
        LocTemp[:,j] = loctemp
    
    #Concat all the data into one dataframe
    timeDF = pd.DataFrame(time,columns=['Time'])
    AveTempDF = pd.DataFrame(AveTemp,columns=['AveTemp'])
    LocTempDF = pd.DataFrame(LocTemp,columns=['LocTemp1','LocTemp2','LocTemp3','LocTemp4',
                                              'LocTemp5','LocTemp6','LocTemp7','LocTemp8',
                                              'LocTemp9','LocTemp10','LocTemp11','LocTemp12',
                                              'LocTemp13','LocTemp14','LocTemp15','LocTemp16'])
    Data = pd.concat([timeDF,AveTempDF,LocTempDF],axis=1)
    UniqueData = Data.drop_duplicates('Time')
    UniqueData.to_csv(ResultPath+'/Summary/Dymola Results_Scenario-'+str(ScenarioNum+1)+'.csv')
    
    ##Identify the parameters of transfer function
    # Create a dataframe to restore the P I parameters
    K = np.zeros([1,17])
    T1 = np.zeros([1,17])
    T2 = np.zeros([1,17])
    D = np.zeros([1,17])
    K_DF = pd.DataFrame(K,columns=list(UniqueData.iloc[:,1:18]))
    T1_DF = pd.DataFrame(T1,columns=list(UniqueData.iloc[:,1:18]))
    T2_DF = pd.DataFrame(T2,columns=list(UniqueData.iloc[:,1:18]))
    D_DF = pd.DataFrame(D,columns=list(UniqueData.iloc[:,1:18]))
    parameters_DF = pd.concat([K_DF,T1_DF,T2_DF,D_DF],axis=0)
    parameters_DF.index = ['K','T1','T2','D']
    # Identify K, T1, T2, D
    for j in range(1):
        [k1,t11,t21,d1,fopt1] = transferFunctionPython(repoPath,UniqueData.iloc[:,[0,j+1]],-0.7,800,400,5)
        [k2,t12,t22,d2,fopt2] = transferFunctionPython(repoPath,UniqueData.iloc[:,[0,j+1]],0.7,2000,400,5)
        kopt = np.mean([k1,k2])
        t1opt = np.mean([t11,t12])
        t2opt = np.mean([t21,t22])
        dopt = np.mean([d1,d2])
        # Save P I into the dataframe
        parameters_DF.iloc[:,j] = [kopt,t1opt,t2opt,dopt]
#        comparisonPython(kopt,t1opt,t2opt,dopt,-0.7,UniqueData.iloc[:,[0,j+1]],800,400,5)
#        comparisonPython(kopt,t1opt,t2opt,dopt,0.7,UniqueData.iloc[:,[0,j+1]],2000,400,5)
    # Save PI_DF 
    parameters_DF.to_csv(ResultPath+'/Summary/Transfer Function_Scenario-'+str(ScenarioNum+1)+'.csv')
     
    output.put()
#    #Loop in scnearios
#        
#    for i in range(ScenarioNum):
#        #Creat dataframe for this scenario
#        #Read time and temperature data
#        ResultFile = os.path.join(ResultPath,"Scenario-"+str(i+1),
#                                  "OpenLoopTestwithUncertainty_readExtTXT_CHEN.mat")
#        time = []
#        AveTemp = []
#        r = Reader(ResultFile,"dymola")
#        (time,AveTemp) = r.values('roo.yCFD[1]')
#        LocTemp = np.zeros([np.size(time,axis=0),16])
#        #Loop in 16 local temperature sensors
#        for j in range(16):
#            (time,loctemp) = r.values('roo.yCFD['+str(j+3)+']')
#            LocTemp[:,j] = loctemp
#        
#        #Concat all the data into one dataframe
#        timeDF = pd.DataFrame(time,columns=['Time'])
#        AveTempDF = pd.DataFrame(AveTemp,columns=['AveTemp'])
#        LocTempDF = pd.DataFrame(LocTemp,columns=['LocTemp1','LocTemp2','LocTemp3','LocTemp4',
#                                                  'LocTemp5','LocTemp6','LocTemp7','LocTemp8',
#                                                  'LocTemp9','LocTemp10','LocTemp11','LocTemp12',
#                                                  'LocTemp13','LocTemp14','LocTemp15','LocTemp16'])
#        Data = pd.concat([timeDF,AveTempDF,LocTempDF],axis=1)
#        UniqueData = Data.drop_duplicates('Time')
#        UniqueData.to_csv(ResultPath+'/Summary/Dymola Results_Scenario-'+str(i+1)+'.csv')
#        
#        ##Identify the parameters of transfer function
#        # Create a dataframe to restore the P I parameters
#        K = np.zeros([1,17])
#        T1 = np.zeros([1,17])
#        T2 = np.zeros([1,17])
#        D = np.zeros([1,17])
#        K_DF = pd.DataFrame(K,columns=list(UniqueData.iloc[:,1:18]))
#        T1_DF = pd.DataFrame(T1,columns=list(UniqueData.iloc[:,1:18]))
#        T2_DF = pd.DataFrame(T2,columns=list(UniqueData.iloc[:,1:18]))
#        D_DF = pd.DataFrame(D,columns=list(UniqueData.iloc[:,1:18]))
#        parameters_DF = pd.concat([K_DF,T1_DF,T2_DF,D_DF],axis=0)
#        parameters_DF.index = ['K','T1','T2','D']
#        # Identify K, T1, T2, D
#        for j in range(17):
#            [k,t1,t2,d,fopt] = transferFunctionPython(repoPath,UniqueData.iloc[:,[0,j+1]],-0.7,800,400,5)
#            # Save P I into the dataframe
#            parameters_DF.iloc[:,j] = [k,t1,t2,d]
#    #    # Save PI_DF 
#        parameters_DF.to_csv(ResultPath+'/Summary/Transfer Function_Scenario-'+str(i+1)+'.csv')
#    Time = timeit.default_timer()-Start

def parallelPSO(Num):
    output = mp.Queue()
    processes = [mp.Process(target=optimization,args = (repoPath,i,"OpenLoopTestwithUncertainty_readExtTXT_CHEN.mat", output)) for i in range(Num)]
    
    # Count the number of CPU
    cpu = mp.cpu_count()
    print cpu
    
    model_results = []
    
    run_times = math.floor(len(processes)/cpu)
    if run_times > 0:
        for i in range(int(run_times)):
            for p in processes[i*int(cpu):(i+1)*int(cpu)]:
                p.start()
            
            for p in processes[i*int(cpu):(i+1)*int(cpu)]:
                p.join()
    
    for p in processes[int(run_times)*int(cpu):len(processes)]:
        p.start()
            
    for p in processes[int(run_times)*int(cpu):len(processes)]:
        p.join() 
    
    #get the outputs
    temp = [output.get() for p in processes[int(run_times)*int(cpu):len(processes)]]
    for x in temp:
        model_results.append(x)
        
    return model_results

if __name__ == "__main__":   

    ScenarioNum = 2
    
    repoPath = os.getcwd()
    ResultPath = repoPath+'/Results'
    
    #Purge old results
    if os.path.exists(ResultPath+'/Summary'):
        shutil.rmtree(ResultPath+'/Summary')
        os.mkdir(ResultPath+'/Summary')
    else:
        os.mkdir(ResultPath+'/Summary')
        
    print parallelPSO(2)












































